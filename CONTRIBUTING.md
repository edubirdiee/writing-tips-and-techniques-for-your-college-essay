<h1>Writing tips and techniques for your college essay</h1>

<br>How do I make my college essay stand out or what are some tips and strategies for college essay writing are just a few questions we sampled out of the mountains of questions we get in our inbox concerning college <a href="http://www.uky.edu/~kecall2/welcome_files/Introducing the College Essay Model.htm">application essay writing</a>.</br>
<br>Undoubtedly, the college essay is a place to showcase your writing prowess. It’s also the part of a college application you’re required to shine your voice. Yes, the college essay matters. It reveals important aspects of yourself your grades and test scores can’t. The college essay can give admissions officers an in-depth picture of who you are while demonstrating your writing ability.</br>
<br>But writing a college essay is a tough task. But you don’t have to worry about it because this article will teach you some useful tips to help make the college application essay writing process simple for you.</br>
<br>Let’s get into it.</br>
<br><b>Get the Essay Structure Right</b></br>
While the content of the college essay matters, how you present your arguments, thoughts, and views matters a lot. The instructions given might state the structure to be followed, but you also need to know how to format different types of essays.  Start familiarizing yourself with the  <a href="https://edubirdie.com/blog/essay-structure">essay basic structure</a> of different essays before you start writing your college essay. This will help you know how to present your ideas logically and coherently to make your essay convincing.
<br><b>Show, Don’t Tell</b></br>
There is a difference between saying you were able to help businesses increase their sales as a sales representative and actually stating the figures that show the increase in sales. This is an example of showing you achievements. 
To write a college essay that stands out, you need to show how you were able to do or achieve something commendable instead of just saying you did something commendable.
<b><br>Don’t Write Too Much About Your Past</b></br>
Your college application essay needs to show growth and introspection. It should show the admissions officers that you’re learning and growing as a person. It’s not advised to brag too much but some little bragging can play the trick. Show that you’re constantly improving through learning by avoiding too much of your past life.
<b><br>You’re Free to be Self-Centered</b></br>
<br>When it comes to college essay writing, most senior high school students have a hard time being self-centered. They focus on writing about an experience, another person, their favorite activities, etc. instead of writing about their personality, quirks, or passions.</br>
<br>You’d be forgiven for this because up until the time of writing your college essay, you’ve been writing essays on books you’ve read or concepts you learned.</br>
<br>However, when writing your college application, you need to write more about yourself as a person. What kind of teammate are you? Are your past failures the reason you have achieved your success academically? Speak about things that have helped you as a person succeed academically and in your personal life.</br>
<b><br>Write the Way You Speak</b></br>
<br>Your college essay needs to be conversational. The admissions officer reading your application needs to feel your <a href="http://facultyweb.ivcc.edu/rrambo/tip_formal_writing_voice.htm">conversational tone</a> in your writing. They need to get a picture of the person who will be around their campus in case you’re admitted. You’re, therefore, required to provide examples of the student you are by how you say about yourself.</br>


